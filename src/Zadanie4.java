import javax.sound.sampled.Line;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
class Figure {
    String label;

}
class Point extends Figure {
    private double x, y;
    public Point(){}
    public Point(double x, double y){this.x = x; this.y = y;}
    public Point(Point p){ this.x = p.x; this.y = p.y;}
    public void move(double x, double y){ this.x+= x; this.y += y;}
    public String toString(){ return "x = "+this.x+" y = "+this.y;}
    public float getArea(){return 0;}

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
class Section extends Figure {
    private Point p1, p2;
    public Section(){}
    public Section(Point p1,Point p2){this.p1 = p1; this.p2 = p2;}
    public Section(Section s){ this.p1 = s.p1; this.p2 = s.p2;}
    public void move(double x, double y){
        p1.move(x,y);
        p2.move(x,y);
    }
    public String toString(){ return p1.toString()+ " "+ p2.toString();}
    public float getArea(){return 0;}

    public Point getP1() {
        return p1;
    }

    public Point getP2() {
        return p2;
    }
}
class Circle extends Figure{
    private Point s;
    private float r;
    public Circle(){}
    public Circle(Point p, float r){this.s = p; this.r = r;}
    public Circle(Circle c){this.s = c.s; this.r = c.r;}
    public void move(double x, double y){ s.move(x,y);}
    public String toString(){return "kolo ma srodek w punktcie "+ this.s.toString() + " i promien "+ this.r;}
    public float getArea(){return (float)(3.14*this.r*this.r);}

    public Point getS() {
        return s;
    }

    public float getR() {
        return r;
    }
}
class Pictures extends Figure{
    List<Object> objects = new ArrayList<Object>();


    public void addPoint(Point b) { objects.add(b); }

    public Object getPoint(int i) {
        if (i>=0 && i<objects.size())
            return objects.get(i);
        return null;
    }
    public void removeObject(int i) {
        if (i>=0 && i<objects.size())
            objects.remove(i);
    }
    public void addSection(Section b) { objects.add(b); }

    public Object getSection(int i) {
        if (i>=0 && i<objects.size())
            return objects.get(i);
        return null;
    }
    public void removeSection(int i) {
        if (i>=0 && i<objects.size())
            objects.remove(i);
    }
    public void addCircle(Circle b) { objects.add(b); }

    public Object getCircle(int i) {
        if (i>=0 && i<objects.size())
            return objects.get(i);
        return null;
    }
    public void removeCircles(int i) {
        if (i>=0 && i<objects.size())
            objects.remove(i);
    }
    public void move(double x, double y){
        for(Object p: objects)
        {
            if(p instanceof Point){
                Point pkt =(Point)p;
                pkt.move(x,y);
                p = pkt;
            }
            else if(p instanceof Section){
                Section sec =(Section)p;
                sec.move(x,y);
                p = sec;
            }
            else if(p instanceof Circle){
                Circle cir =(Circle)p;
                cir.move(x,y);
                p = cir;
            }
        }



    }
    public String toString() {
        List<Point> points = new ArrayList<Point>();
        List<Section> sections = new ArrayList<Section>();
        List<Circle> circles = new ArrayList<Circle>();
        for (Object p : objects) {
            if (p instanceof Point) {
                Point pkt = (Point) p;
                points.add(pkt);
            } else if (p instanceof Section) {
                Section sec = (Section) p;
                sections.add(sec);
            } else if (p instanceof Circle) {
                Circle cir = (Circle) p;
                circles.add(cir);

            }
            System.out.println("Punkty:");
            for (Point pk : points)
                System.out.println(pk.toString());
            System.out.println("Odcinki:");
            for (Section s : sections)
                System.out.println(s.toString());
            System.out.println("Koła:");
            for (Circle c : circles)
                System.out.println(c.toString());


        }
        return "";
    }
    public void sumaPol(){

        double sPoint=0, sSection=0,sCircle=0;
        for(Object o: objects){
            if(o instanceof Point){
                Point p =(Point)o;
                sPoint += p.getX() + p.getY();

            }
            else if(o instanceof Section){
                Section s =(Section)o;
                sSection +=s.getP1().getX()+s.getP1().getY()+s.getP2().getX()+s.getP2().getY();
            }
            else if(o instanceof Circle){
                Circle c =(Circle)o;
                sCircle+= c.getS().getX()+c.getS().getY()+c.getR();
            }

        }





        System.out.println("suma wszystkich pol obiektow Point: "+sPoint);
        System.out.println("suma wszystkich pol obiektow Section: "+sSection);
        System.out.println("suma wszystkich pol obiektow Circle: "+sCircle);

    }



    public void getArea(){
        for(Object o: objects)
            if(o instanceof Circle){
                Circle c = (Circle)o;
                System.out.println(c.getArea());
            }



    }
}



public class Zadanie4 {
    public static void main(String[] args) {
        boolean flaga = true;
        Scanner scan = new Scanner(System.in);
        int zmienna;
        Pictures pictures = new Pictures();

        while(flaga){
            System.out.println("1. Dodaj punkt.");
            System.out.println("2. Dodaj wektor.");
            System.out.println("3. Dodaj Koło.");
            System.out.println("4. Wyswietl wszystoko.");
            System.out.println("5. Przesun.");
            System.out.println("6. Wyswietl sume pol.");
            System.out.println("7. Zakoncz.");
            zmienna = scan.nextInt();

            switch (zmienna) {
                case 1: {
                    System.out.println("podaj x punktu");
                    double x = scan.nextDouble();
                    System.out.println("podaj y punktu");
                    double y = scan.nextDouble();

                    pictures.addPoint(new Point(x, y));
                    break;
                }
                case 2: {
                    System.out.println("podaj x pierwszego punktu");
                    double x1 = scan.nextDouble();
                    System.out.println("podaj y pierwszego punktu");
                    double y1 = scan.nextDouble();
                    System.out.println("podaj x drugiego punktu");
                    double x2 = scan.nextDouble();
                    System.out.println("podaj y drugiego punktu");
                    double y2 = scan.nextDouble();
                    pictures.addSection(new Section(new Point(x1,y1), new Point(x2,y2)));
                    break;
                }
                case 3: {
                    System.out.println("podaj x punktu");
                    double x = scan.nextDouble();
                    System.out.println("podaj y punktu");
                    double y = scan.nextDouble();
                    System.out.println("podaj promien");
                    float r = scan.nextFloat();
                    pictures.addCircle(new Circle(new Point(x,y),r));
                    break;

                }
                case 4:{
                    pictures.toString();
                    break;
                }
                case 5:{
                    System.out.println("podaj o wartosc przesuniecia dla osi x");
                    double x = scan.nextDouble();
                    System.out.println("podaj o wartosc przesuniecia dla osi y");
                    double y = scan.nextDouble();
                    pictures.move(x,y);
                    break;
                }
                case 6:{
                    pictures.sumaPol();
                }
                case 7:{
                    flaga=false;
                }




            }



        }

    }

}
